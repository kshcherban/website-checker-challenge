FROM python:3.9-slim as builder

RUN pip install poetry==1.1.6

WORKDIR /app

ENV POETRY_VIRTUALENVS_IN_PROJECT=true \
    POETRY_NO_INTERACTION=1 \
    PYTHONDONTWRITEBYTECODE=1

COPY . /app

RUN poetry install --no-dev


FROM python:3.9-slim

WORKDIR /app

ENV PYTHONUNBUFFERED=1

COPY --from=builder /app /app

ENTRYPOINT [".venv/bin/python", "main.py"]
