# Website checker solution

## Description

Program that checks website availability and reports it to Postgres via Kafka.

Kafka producer which periodically checks the target websites and
sends the check results to a Kafka topic, and a Kafka consumer storing the data to a
PostgreSQL database.

Both kafka producer and consumer run infinetly, check rules are configured in specified
yaml configuration file.

## Requirements

Python >= 3.8, poetry, docker.

To install requirements run:
```bash
pip3 install poetry
poetry install
```

## Usage

To test locally run in different terminals:

```bash
# start local services
docker-compose up postgres kafka
# activate python environment
poetry shell
# create database table
python main.py --init-db
# start consumer first
python main.py --consumer
# start producer
python main.py --producer
```

Alternatively you can run everything in docker:

```bash
docker-compose up --build
```

Services use rules configuration from yaml file, check `config.yml` for example.
Also services are configured via environment variables, here's a full list of them with default values:

```bash
CHECK_RULES_CONFIG="config.yml"
POSTGRES_URL="postgresql://postgres:pgpass@postgres:5432/website_checker"
KAFKA_URL="localhost:9092"
KAFKA_USE_SSL="no"
KAFKA_TOPIC="website_checker"
KAFKA_CA_FILE="ca.pem"
KAFKA_CERT_FILE="service.cert"
KAFKA_KEY_FILE="service.key"
```

## Testing

Some tests depend on running postgres database. To run tests:

```bash
docker-compose up postgres
pytest
```
