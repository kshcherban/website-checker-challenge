#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import time
import argparse
import asyncio

from website_checker import config
from website_checker import web
from website_checker import postgres
from website_checker import kafka


def parse_args():
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "--producer",
        action="store_true",
        help="run kafka producer mode, will perform checks",
    )
    group.add_argument(
        "--consumer",
        action="store_true",
        help="run kafka consumer mode, will write checks in db",
    )
    group.add_argument("--init-db", action="store_true", help="create database table")
    return parser, parser.parse_args()


if __name__ == "__main__":
    parser, args = parse_args()
    # Retrieve configuration from environment
    # Other configuration setup is done in kafka and postgres files respectively
    config_file = os.environ.get("CHECK_RULES_CONFIG", "config.yml")
    log = config.log

    if args.init_db:
        postgres.create_tables()
    elif args.producer:
        log.info(f"running in producer mode, config: {config_file}")
        rules_config = config.read(config_file)
        interval = rules_config.get("interval", 60)
        asyncio.run(web.run_checks(interval, rules_config["rules"]))
    elif args.consumer:
        asyncio.run(kafka.consume_messages())
    else:
        parser.print_help()
