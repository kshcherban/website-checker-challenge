from website_checker import config


def test_read_config():
    conf = config.read("config.yml")

    assert conf["interval"] == 10
    assert "rules" in conf
    assert conf["rules"][0]["url"] == "http://example.com"
