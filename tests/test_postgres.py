import os
import asyncio
import psycopg2
from datetime import timezone

# overwrite env to ensure we're connected to test db
db_url = "postgresql://postgres:pgpass@localhost:5432/website_checker"
os.environ["POSTGRES_URL"] = db_url

from website_checker import postgres


def test_create_tables():
    postgres.create_tables()
    conn = psycopg2.connect(db_url)
    cur = conn.cursor()
    cur.execute("select * from information_schema.tables where table_name='checks'")
    assert bool(cur.rowcount) is True


def test_write_message():
    message = {
        "url": "http://foo",
        "pattern": True,
        "status": 200,
        "checked_at": 1621543365,
        "elapsed": 1,
    }
    loop = asyncio.get_event_loop()
    pool = loop.run_until_complete(postgres.init_pool())
    loop.run_until_complete(postgres.write(message, pool))
    conn = psycopg2.connect(db_url)
    cur = conn.cursor()
    cur.execute("select checked_at from checks where url = 'http://foo'")
    result = cur.fetchone()[0]
    timestamp = result.replace(tzinfo=timezone.utc).timestamp()
    assert timestamp == message["checked_at"]
