import unittest
from unittest.mock import patch

import asyncio
import aiohttp

from website_checker import web


async def _run_checks():
    rules = [{"url": "http://google.com", "regexp": "search"}]
    tasks = []
    async with aiohttp.ClientSession() as session:
        for rule in rules:
            url, regexp = rule["url"], rule.get("regexp")
            tasks.append(web.fetch(session, url, regexp, 5))
        _ = await asyncio.gather(*tasks)


class Test(unittest.TestCase):
    @patch("website_checker.kafka.send_message")
    def test_web(self, send_message_mock):
        send_message_mock.return_value = "mocked"
        asyncio.run(_run_checks())
        assert send_message_mock.call_args.args[0]["status"] == 200
        assert send_message_mock.call_args.args[0]["pattern"] is True
