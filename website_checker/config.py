import sys
import logging
from yaml import load, dump

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


def init_logger():
    logger = logging.getLogger()
    console_handler = logging.StreamHandler()
    log_formatter = logging.Formatter(
        fmt="%(asctime)s.%(msecs)03d: %(levelname)s: %(message)s",
        datefmt="%Y-%m-%d,%H:%M:%S",
    )
    console_handler.setFormatter(log_formatter)
    logger.addHandler(console_handler)
    logger.setLevel(logging.INFO)
    return logger


# Define reusable logger
log = init_logger()


def _validate(data) -> None:
    try:
        for url in data["rules"]:
            url["url"]
    except KeyError:
        log.critical("Config has incorrect format")
        sys.exit(1)


def read(config_file: str) -> dict:
    with open(config_file, "r") as stream:
        parsed = load(stream, Loader=Loader)
    _validate(parsed)
    return parsed
