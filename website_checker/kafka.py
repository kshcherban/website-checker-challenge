import os
import json

from aiokafka import AIOKafkaProducer, AIOKafkaConsumer
from aiokafka.helpers import create_ssl_context

from website_checker import config
from website_checker import postgres


log = config.log

kafka_url = os.environ.get("KAFKA_URL", "localhost:9092")
kafka_use_cert = os.environ.get("KAFKA_USE_SSL", "no")
kafka_ca_file = os.environ.get("KAFKA_CA_FILE", "ca.pem")
kafka_cert_file = os.environ.get("KAFKA_CERT_FILE", "service.cert")
kafka_key_file = os.environ.get("KAFKA_KEY_FILE", "service.key")
kafka_topic = os.environ.get("KAFKA_TOPIC", "website_checker")

if kafka_use_cert != "no":
    context = create_ssl_context(
        cafile=kafka_ca_file,
        certfile=kafka_cert_file,
        keyfile=kafka_key_file,
    )
    protocol = "SSL"
else:
    context = None
    protocol = "PLAINTEXT"


async def send_message(message) -> None:
    log.info(f'storing message in kafka for url: {message["url"]}')
    producer = AIOKafkaProducer(
        bootstrap_servers=kafka_url, security_protocol=protocol, ssl_context=context
    )
    await producer.start()
    try:
        await producer.send_and_wait(kafka_topic, json.dumps(message).encode())
    except Exception:
        log.exception(f'failed to send message for url: {message["url"]}')
    finally:
        await producer.stop()


async def consume_messages() -> None:
    consumer = AIOKafkaConsumer(
        kafka_topic,
        bootstrap_servers=kafka_url,
        security_protocol=protocol,
        ssl_context=context,
        metadata_max_age_ms=10000,  # reconnect faster, default is 300s
    )
    await consumer.start()
    try:
        db_pool = await postgres.init_pool()
        async for msg in consumer:
            message = json.loads(msg.value)
            log.info(f'storing message in db for url: {message["url"]}')
            await postgres.write(message, db_pool)
    finally:
        await consumer.stop()
