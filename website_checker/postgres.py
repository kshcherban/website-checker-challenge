import os
import aiopg
import psycopg2

from datetime import datetime

from website_checker import config


log = config.log

postgres_url = os.environ.get(
    "POSTGRES_URL", "postgresql://postgres:pgpass@localhost:5432/website_checker"
)


def create_tables() -> None:
    table = """
        CREATE TABLE IF NOT EXISTS checks (
            id SERIAL,
            status INT,
            elapsed FLOAT,
            pattern BOOL,
            checked_at TIMESTAMP,
            url TEXT
        );
    """
    log.info("connecting to postgres database")
    conn = psycopg2.connect(postgres_url)
    cur = conn.cursor()
    log.info("creating table")
    cur.execute(table)
    conn.commit()


async def init_pool():
    try:
        pool = await aiopg.create_pool(postgres_url)
        log.info("connected to postgres database")
        return pool
    except Exception:
        log.exception("failed to connect to postgres db")


async def write(message: str, pool: aiopg.pool.Pool) -> None:
    try:
        timestamp = datetime.utcfromtimestamp(message["checked_at"])
        if message["pattern"] is None:
            pattern = "NULL"
        else:
            pattern = message["pattern"]
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(
                    f'INSERT INTO checks (status, elapsed, pattern, checked_at, url) VALUES ({message["status"]}, {message["elapsed"]}, {pattern}, \'{timestamp}\', \'{message["url"]}\')',
                )
                log.info(f'written message into database for url: {message["url"]}')
    except Exception:
        log.exception(f'failed to write into database url: {message["url"]}')
