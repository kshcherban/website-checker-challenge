import re
import time
import aiohttp
import asyncio

from website_checker import config
from website_checker import kafka


log = config.log


def pattern_present(content: str, pattern: str) -> bool:
    pattern_compiled = re.compile(pattern)
    return bool(pattern_compiled.search(content))


async def fetch(session, url: str, pattern: str, timeout: int) -> None:
    timeout = aiohttp.ClientTimeout(total=timeout)
    start_time = time.time()
    try:
        async with session.get(url, timeout=timeout) as response:
            html, status = await response.text(), response.status
            end_time = time.time()
            elapsed_time = end_time - start_time
            if pattern:
                pattern_found = pattern_present(html, pattern)
            else:
                pattern_found = None
            message = {
                "status": status,
                "elapsed": elapsed_time,
                "pattern": pattern_found,
                "url": url,
                "checked_at": end_time,
            }
            # send message to kafka ASAP
            await kafka.send_message(message)
    except Exception:
        log.exception(f"failed to check url: {url}")


async def run_checks(interval: int, rules: list) -> None:
    while True:
        tasks = []
        async with aiohttp.ClientSession() as session:
            for rule in rules:
                url, regexp = rule["url"], rule.get("regexp")
                tasks.append(fetch(session, url, regexp, interval - 1))
            _ = await asyncio.gather(*tasks)
        await asyncio.sleep(interval)
